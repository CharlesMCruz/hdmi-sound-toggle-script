# README #

Link to my blog with instructions:
http://charlesmcruz.wordpress.com/2012/06/23/ubuntu-12-04-stoggle-hdmi-sound-toggleswitch/

### How to setup this script: ###

1.  In a terminal:
sudo gedit /etc/udev/rules.d/hdmi.rules
2. Copy, paste, then save:
SUBSYSTEM=="drm", ACTION=="change", RUN+="/usr/local/bin/SToggle"
3. In the terminal:
sudo udevadm control --reload-rules
4. In the terminal:
sudo gedit /usr/local/bin/SToggle
5. Copy and paste the toggle-sound script and then save it.
6. In the terminal:
sudo chmod 755 /usr/local/bin/SToggle